package no.uib.inf101.datastructure;


import java.util.List;

/**
 * Objects in a class implementing CellColorCollection can assemble a list
 * of GridCell<Color> objects through the getCells() method.
 * 
 * @param <T> Typen til elementene som GridCell-ene inneholder.
 */
public interface GridCellCollection  <T>  {

  /**
   * Get a list containing the GridCell<Color> objects in this collection
   *
   * @return a list of all GridCell<Color> objects in this collection
   */
  List<GridCell<T>> getCells();

}
