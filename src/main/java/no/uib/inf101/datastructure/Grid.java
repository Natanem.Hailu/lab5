package no.uib.inf101.datastructure;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementerer en 2D-struktur av generiske elementer.
 *
 * @param <T> Typen til elementene gridet skal inneholde.
 */
public class Grid<T> implements IGrid<T> {

    private final List<List<T>> data;
    private final int cols;
    private final int rows;

    public Grid(int rows, int cols, T defaultValue) {
        this.rows = rows;
        this.cols = cols;
        data = new ArrayList<>(rows);

        for (int i = 0; i < rows; i++) {
            List<T> row = new ArrayList<>(cols);
            for (int j = 0; j < cols; j++) {
                row.add(defaultValue);
            }
            data.add(row);
        }
    }

    public Grid(int rows, int cols) {
        this(rows, cols, null);
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return cols;
    }

    @Override
    public List<GridCell<T>> getCells() {
        List<GridCell<T>> cells = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                CellPosition pos = new CellPosition(row, col);
                cells.add(new GridCell<>(pos, data.get(row).get(col)));
            }
        }
        return cells;
    }

    @Override
    public T get(CellPosition pos) {
        return data.get(pos.row()).get(pos.col());
    }

    @Override
    public void set(CellPosition pos, T elem) {
        data.get(pos.row()).set(pos.col(), elem);
    }
}
